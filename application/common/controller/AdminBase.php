<?php
namespace app\common\controller;
use think\Request;
use think\Controller;
use think\Session;
use think\Db;
class AdminBase extends controller{
	
	public function _initialize()
	{
		# 管理员ID
		$admin_id = session('admin_user.id');

		$request = Request::instance();
		# 当前操作的模型名
		$module_name = $request->module(); 
		# 当前操作的控制器名
		$controller_name = $request->controller();
		# 当前操作的方法名
		$action_name = $request->action(); 
		# 实例化auth权限认证类
		$auth=new \think\Auth();
		$rule_name=$module_name.'/'.$controller_name.'/'.$action_name;
		$result=$auth->check($rule_name,$admin_id);

		if(!$result){
			$this->error('您没有权限访问', url('Index/index'));
		}
	}
	
}