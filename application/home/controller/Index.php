<?php
namespace app\home\controller;
use app\common\controller\HomeBase;
use Hooklife\ThinkphpWechat\Wechat;
use think\Db;
use think\Session;

class Index extends HomeBase
{
    public function index()
    {
    	$user=Session::get('wechat_user');
	    //将用户的基本信息保存在数据库中，然后提供下次进行使用
	    dump($user);die;
        return $this->fetch('Index/index');
    }

    // 选择角色页面
    public function select_user()
    {
    	return $this->fetch('Index/select_user');
    }

    // 聊天室
    public function web_socket()
    {
    	$uid = $this->request->param('uid');
        Session::set('uid',$uid);
    	return $this->fetch('Index/web_socket',['uid'=>$uid]);
    }

    //用户上传聊天图片
    public function send_chat_img(){
        if (empty($_FILES['file_img'])) return;
        $arr = ['status'=>0];
        # 图片上传
        $file = request()->file('file_img');
        $info = $file->validate(['size'=>1024*1024,'ext'=>'jpg,png,gif'])->move(ROOT_PATH . 'public' . DS . 'upload'. DS . 'chat');
        
        if($info){
            $img = '/upload/chat/'.$info->getSaveName();
            $arr['status'] = 1;
            $arr['img'] = $img;
        }else{
            // 上传失败获取错误信息
            $arr['msg'] = $file->getError();
        }
        return json_encode($arr);
    }

    // 获取redis中存储的在线用户
    public function getOnlineUser()
    {
        return self::RedisObj()->hKeys('client_person');
    }

    // -------------------------------------------
    // Redis对象
    // -------------------------------------------
    static function RedisObj()
    {
        static $redis = null;
        if(is_null($redis)){
            $redis = new Redis();
            $redis->connect('127.0.0.1',6379);
            $redis->auth('yqy1994');
        }
        return $redis;
    }


}