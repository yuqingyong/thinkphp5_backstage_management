<?php
namespace app\weixin\controller;
use think\Controller;
use think\Db;
use think\Request;
use think\Session;
use EasyWeChat\Foundation\Application;
use EasyWeChat\Message\Text;
use EasyWeChat\Message\Image;
use EasyWeChat\Payment\Order;
# 缓存插件
use Doctrine\Common\Cache\RedisCache;
/**
 * easywechat 插件
 * 用于微信各种接口开发
 */
class Easychat extends Controller{
	// 定义服务端服务
	protected $server;
	// 定义用户服务
	protected $userService;
	// oauth
	protected $oauth;
	// 支付
	protected $payment;
	// 二维码
	protected $qrcode;
	// 短网址服务
	protected $url;
	// access_token
	protected $accessToken;
	// js-SDK实例
	protected $js;
	// 卡券实例
	protected $card;

	// 初始化操作
	public function _initialize()
	{
		// 获取配置信息
		$config = Db::name('weixin')->where('status',1)->field('appid,appsecret,mchid,mchkey,status')->find();

		$options = [
		    'debug'  => true,
		    'app_id' => $config['appid'],
		    'secret' => $config['appsecret'],
		    'token'  => 'yuqingyong',
		    // 'aes_key' => null, // 可选
		    'log' => [
		        'level' => 'debug',
		        'file'  => './material.log',
		    ],
		    'oauth' => [
		        'scopes'   => ['snsapi_userinfo'],
			    'callback' => '/weixin/Easychat/oauth_callback',
		  	],
		  	'payment' => [
		        'merchant_id'        => '商户ID',
		        'key'                => '商户签名key',
		        'cert_path'          => '/extend/wxpay/cert/apiclient_cert.pem', // XXX: 绝对路径！！！！
		        'key_path'           => '/extend/wxpay/cert/apiclient_key.pem',      // XXX: 绝对路径！！！！
		        'notify_url'         => '/weixin/Easychat/payNotify',    // 你也可以在下单时单独设置来想覆盖它
		    ],
		];
		$app = new Application($options);

		$this->server = $app->server;
		$this->userService = $app->user;
		$this->oauth = $app->oauth;
		$this->payment = $app->payment;
		$this->qrcode = $app->qrcode;
		$this->url = $app->url;
		$this->accessToken = $app->access_token;
		$this->js = $app->js;
		$this->card = $app->card;
	}

	// 微信消息接收和返回
	public function event(){
		$this->server->setMessageHandler(function($message){
			switch ($message->MsgType) {
		        case 'event':
		            return $this->dealEvent($message);
		            break;
		        case 'text':
		            return $this->ContentText();
		            break;
		        case 'image':
		            return $this->ImageText();
		            break;
		        case 'voice':
		            return '收到语音消息';
		            break;
		        case 'video':
		            return '收到视频消息';
		            break;
		        case 'location':
		            return '收到坐标消息';
		            break;
		        case 'link':
		            return '收到链接消息';
		            break;
		        // ... 其它消息
		        default:
		            return '收到其它消息';
		            break;
		    }
		});
		// $message = $this->server->getMessage();
		// $message->ToUserName    接收方帐号（该公众号 ID）
		// $message->FromUserName  发送方帐号（OpenID, 代表用户的唯一标识）
		// $message->CreateTime    消息创建时间（时间戳）
		// $message->MsgId         消息 ID（64位整型）

		$response = $this->server->serve();
		// 将响应输出
		$response->send(); // Laravel 里请使用：return $response;
	}

	// 获取access_token
	public function getAccessToken()
	{
		$token = $this->accessToken->getToken(); // token 字符串
		// $token = $this->accessToken->getToken(true); // 强制重新从微信服务器获取 token.
		# 修改 $app 的 Access Token
		// $app['access_token']->setToken($newAccessToken, $expires);
	}

	// 文本消息
	public function ContentText()
	{
		// $text = new Text(['content'=>'您好！星辰网络博客1！']);

		// or
		$text = new Text();
		// $text->content = '您好！星辰网络博客2！';

		// or
		$text->setAttribute('content','您好！星辰网络博客3！');
		return $text;
	}

	// 图片消息
	public function ImageText()
	{
		$image = new Image(['media_id'=>$mediaId]);

		//or
		// $image = new Image();
		// $image->media_id = $mediaId;

		// or
		// $image = new Image();
		// $image->setAttribute('media_id',$mediaId);
		return $image;
	}

	// 获取用户信息
	public function getUserInfo()
	{
		$user = $this->userService->get('oHHjv0mVJeHys1PxpaHYXgN1SafI');
		dump($user);die;
	}

	// 转发收到的消息给客服
	public function messageToService()
	{
		# 给所有客服
		$this->server->setMessageHandler(function($message) {
		    return new \EasyWeChat\Message\Transfer();
		});

		# 给指定客服
		// $server->setMessageHandler(function($message) {
		//     $transfer = new \EasyWeChat\Message\Transfer();
		//     $transfer->account($account);// 或者 $transfer->to($account);
		//     return $transfer;
		// });
	}

	// 事件消息处理
	public function dealEvent($message)
	{
		if($message->MsgType == 'event'){
			switch ($message->Event) {
				case 'CLICK':
					return '请选择要播放的歌曲';
					break;
				
				default:
					return '收到事件消息';
					break;
			}
		}
	}

	// 微信授权获取用户信息
	public function authorizedLogin()
	{
		if (empty(Session::get('wechat_user'))){
            Session::set('target_url', '/weixin/Easychat/authorizedLogin');
            return $this->oauth->redirect();
        }
        # 获取用户信息
        $user = Session::get('wechat_user');
        dump($user);die;
	}

	// 授权回调
	public function oauth_callback()
	{
		# 获取用户信息
		$user = $this->oauth->user();
		# session存储用户信息
		Session::set('wechat_user',$user);
		# 如果未获取到用户信息
        $targetUrl =empty(Session::get('target_url')) ? '/home/Index/index' : Session::get('target_url');
        header('location:'. $targetUrl); // 跳转到 user/profile
	}

	// ----------------------------------
	// 微信公众号支付
	// ----------------------------------

	public function wxPay()
	{
		# 查询授权openid
		$response = $this->payment->authCodeToOpenId($authCode);
		$openid = $response->openid;

		# 下单数组
		$attributes = [
		    'trade_type'       => 'JSAPI', // JSAPI，NATIVE，APP...
		    'body'             => 'iPad mini 16G 白色',
		    'detail'           => 'iPad mini 16G 白色',
		    'out_trade_no'     => '1217752501201407033233368018',
		    'total_fee'        => 5388, // 单位：分
		    // 'notify_url'       => 'http://xxx.com/order-notify', // 支付结果通知网址，如果不设置则会使用配置里的默认地址
		    'openid'           => '当前用户的 openid', // trade_type=JSAPI，此参数必传，用户在商户appid下的唯一标识，
		    // ...
		];

		# 统一下单
		$order = new Order($attributes);
		$result = $this->payment->prepare($order);
		if ($result->return_code == 'SUCCESS' && $result->result_code == 'SUCCESS'){
		    $prepayId = $result->prepay_id;
		}
	}

	// 支付回调
	public function payNotify()
	{
		$response = $this->payment->handleNotify(function($notify, $successful){
		    // 使用通知里的 "微信支付订单号" 或者 "商户订单号" 去自己的数据库找到订单
		    $order = $notify->out_trade_no; //查询订单($notify->out_trade_no);

		    if (!$order) { // 如果订单不存在
		        return 'Order not exist.'; // 告诉微信，我已经处理完了，订单没找到，别再通知我了
		    }

		    // 如果订单存在
		    // 检查订单是否已经更新过支付状态
		    if ($order->paid_at) { // 假设订单字段“支付时间”不为空代表已经支付
		        return true; // 已经支付成功了就不再更新了
		    }

		    // 用户是否支付成功
		    if ($successful) {
		        // 不是已经支付状态则修改为已经支付状态
		        $order->paid_at = time(); // 更新支付时间为当前时间
		        $order->status = 'paid';
		    } else { // 用户支付失败
		        $order->status = 'paid_fail';
		    }

		    $order->save(); // 保存订单

		    return true; // 返回处理完成
		});

		$response->send();
	}

	// ---------------------------------
	// 二维码获取
	// ---------------------------------
	// 创建临时二维码
	public function createTemporaryCode()
	{
		$result = $this->qrcode->temporary(56, 6 * 24 * 3600);
		$ticket = $result->ticket;// 或者 $result['ticket']
		$expireSeconds = $result->expire_seconds; // 有效秒数
		$url = $result->url; // 二维码图片解析后的地址，开发者可根据该地址自行生成需要的二维码图片
		dump($url);die;
	}

	// 创建永久二维码
	public function createPermanentCode()
	{
		$result = $this->qrcode->forever(56);// 或者 $qrcode->forever("foo");
		$ticket = $result->ticket; // 或者 $result['ticket']
		// $url = $result->url;
		# 获取二维码地址
		// $url = $this->qrcode->url($ticket);

		# 获取二维码内容
		$url = $this->qrcode->url($ticket);
		$content = file_get_contents($url); // 得到二进制图片内容
		file_put_contents('code.jpg', $content); // 写入文件
	}

	// ---------------------------------------------------
	// 短网址服务
	// 主要使用场景： 开发者用于生成二维码的原链接（商品、支付二维码等）太长导致扫码速度和成功率下降，将原长链接通过此接口转成短链接再生成二维码将大大提升扫码速度和成功率
	// ---------------------------------------------------
	public function changeShortUrl($longurl)
	{
		$shortUrl = $this->url->shorten($longurl);
	}

	// ---------------------------------------------------
	// Redis 缓存
	// ---------------------------------------------------
	# 创建实例
	public function createRedis()
	{
		$redis = new Redis();
		$redis->connect('127.0.0.1', 6379);
		$cacheDriver->setRedis($redis);
		$options = [
		    'debug'  => false,
		    'app_id' => $wechatInfo['app_id'],
		    'secret' => $wechatInfo['app_secret'],
		    'token'  => $wechatInfo['token'],
		    'aes_key' => $wechatInfo['aes_key'], // 可选
		    'cache'   => $cacheDriver,
		];
		$wechatApp = new Application($options);
	}

	// ---------------------------------------------------
	// JS-SDK的使用 官方地址 [ https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421141115 ]
	// ---------------------------------------------------
	public function jssdk()
	{
		# 设置当前方法的地址
		$url = 'http://'.$_SERVER['HTTP_HOST'].'/weixin/Easychat/jssdk';
		$this->js->setUrl($url);
		# 获取js-sdk的配置对象
		$config = $this->js->config(array('onMenuShareAppMessage','scanQRCode','chooseImage'), true);
		# 分配给前端
		return $this->fetch('Easychat/jssdk',['config'=>$config]);
	}

	// ---------------------------------------------------
	// 卡券的使用
	// ---------------------------------------------------
	public function createCard()
	{
		$cardType = 'GROUPON';

	    $baseInfo = [
	        'logo_url' => 'http://mmbiz.qpic.cn/mmbiz/2aJY6aCPatSeibYAyy7yct9zJXL9WsNVL4JdkTbBr184gNWS6nibcA75Hia9CqxicsqjYiaw2xuxYZiaibkmORS2oovdg/0',
	        'brand_name' => '测试商户造梦空间',
	        'code_type' => 'CODE_TYPE_QRCODE',
	        'title' => '测试',
	        'sub_title' => '测试副标题',
	        'color' => 'Color010',
	        'notice' => '测试使用时请出示此券',
	        'service_phone' => '15311931577',
	        'description' => "测试不可与其他优惠同享\n如需团购券发票，请在消费时向商户提出\n店内均可使用，仅限堂食",

	        'date_info' => [
	          'type' => 'DATE_TYPE_FIX_TERM',
	          'fixed_term' => 90, //表示自领取后多少天内有效，不支持填写0
	          'fixed_begin_term' => 0, //表示自领取后多少天开始生效，领取后当天生效填写0。
	        ],

	        'sku' => [
	          'quantity' => '0', //自定义code时设置库存为0
	        ],

	        'location_id_list' => ['461907340'],  //获取门店位置poi_id，具备线下门店的商户为必填

	        'get_limit' => 1,
	        'use_custom_code' => true, //自定义code时必须为true
	        'get_custom_code_mode' => 'GET_CUSTOM_CODE_MODE_DEPOSIT',  //自定义code时设置
	        'bind_openid' => false,
	        'can_share' => true,
	        'can_give_friend' => false,
	        'center_title' => '顶部居中按钮',
	        'center_sub_title' => '按钮下方的wording',
	        'center_url' => 'http://www.qq.com',
	        'custom_url_name' => '立即使用',
	        'custom_url' => 'http://www.qq.com',
	        'custom_url_sub_title' => '6个汉字tips',
	        'promotion_url_name' => '更多优惠',
	        'promotion_url' => 'http://www.qq.com',
	        'source' => '造梦空间',
	      ];

	    $especial = [
	      'deal_detail' => 'deal_detail',
	    ];

	    $result = $this->card->create($cardType, $baseInfo, $especial);

	    # 返回值
	    /*
		 *  object(EasyWeChat\Support\Collection)#199 (1) {
		 *	  ["items":protected] => array(3) {
		 *	    ["errcode"] => int(0)
		 *	    ["errmsg"] => string(2) "ok"
	  	 *	    ["card_id"] => string(28) "pHHjv0pxevqLNFRngEM86pIt7L3Y"
		 *	  }
		 *	}
	     */

	    dump($result);die;
	}

	// 领取卡券
	public function getCodeVoucher()
	{
		$cards = [
		    'action_name' => 'QR_CARD',
		    'expire_seconds' => 1800,
		    'action_info' => [
		      'card' => [
		        'card_id' => 'pHHjv0pxevqLNFRngEM86pIt7L3Y',
		        'is_unique_code' => false,
		        'outer_id' => 1,
		      ],
		    ],
		];

		$result = $this->card->QRCode($cards);
		dump($result);die;
	}




}