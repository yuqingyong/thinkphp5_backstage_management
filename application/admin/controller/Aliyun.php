<?php
namespace app\admin\controller;
use app\common\controller\AdminBase;
use think\Request;
use think\Db;
/**
 * 阿里控制器
 * @author yuqingyong
 */
class Aliyun extends AdminBase
{
    // 短信配置页面
    public function sms_config()
    {
      // 配置列表
      $config_list = Db::name('sms_config')->field('id,sms_name,appid,appsecret,smsid,sign,status')->select();

      $assign = [
        'config_list' => $config_list
      ];
      return $this->fetch('Aliyun/sms_config',$assign);
    }

    // 添加短信
    public function add_smsconfig(Request $request)
    {
      $this->view->engine->layout(false);
      if($request->isPost()){
        $data = $this->request->post();

        $result = $this->validate($data,'Aliyun');
        if(true !== $result){
            // 验证失败 输出错误信息
            $this->error($result);exit;
        }

        $res = Db::name('sms_config')->insert($data);
        if($res){
          $this->success('添加成功',url('Aliyun/sms_config'));
        }
      }
      return $this->fetch('Aliyun/add_smsconfig');
    }

    // 修改短信
    public function edit_smsconfig(Request $request)
    {
      $this->view->engine->layout(false);
      $id = $this->request->param('id');
      $config = Db::name('sms_config')->where('id',$id)->field('id,sms_name,appid,appsecret,smsid,sign')->find();
      if($request->isPost()){
        $id = $this->request->post('id');
        $data = $this->request->post();
        $result = $this->validate($data,'Aliyun');
        if(true !== $result){
            // 验证失败 输出错误信息
            $this->error($result);exit;
        }

        $res = Db::name('sms_config')->where('id',$id)->update($data);
        if($res){
          $this->success('修改成功',url('Aliyun/sms_config'));
        }
      }
      return $this->fetch('Aliyun/edit_smsconfig',['config'=>$config]);
    }

    // 启用短信
    public function sms_status()
    {
      $id = $this->request->post('id');
      # 需要优先关闭其他通道，再启动当前通道
      Db::name('sms_config')->where('status',1)->update(['status'=>0]);
      $res = Db::name('sms_config')->where('id',$id)->update(['status'=>1]);
      if($res){
        return json_encode(['status'=>1,'msg'=>'启用成功']);
      }else{
        return json_encode(['status'=>0,'msg'=>'启用失败']);
      }
    }

    // 删除短信
    public function del_sms()
    {
      $id = $this->request->post('id');
      # 判断是否有其他替换的公众号
      $else_sms = Db::name('sms_config')->where('id','neq',$id)->field('id')->order('id desc')->find();
      if(empty($else_sms)) return json_encode(['status'=>0,'msg'=>'无替换配置，无法删除']);
      # 如果有替换的，优先开启替换的账号，然后删除
      Db::name('sms_config')->where('id',$else_sms['id'])->update(['status'=>1]);
      $res = Db::name('sms_config')->where('id',$id)->delete();
      if($res){
        return json_encode(['status'=>1,'msg'=>'删除成功']);
      }else{
        return json_encode(['status'=>0,'msg'=>'删除失败']);
      }
    }

    /******************************************支付配置******************************************************/
    // 短信配置页面
    public function pay_config()
    {
      // 配置支付
      $config_list = Db::name('pay_config')->field('id,pay_name,appid,appsecret,mchid,mchkey,status')->select();

      $assign = [
        'config_list' => $config_list
      ];
      return $this->fetch('Aliyun/pay_config',$assign);
    }

    // 添加支付
    public function add_payconfig(Request $request)
    {
      $this->view->engine->layout(false);
      if($request->isPost()){
        $data = $this->request->post();

        $result = $this->validate($data,'Aliyun');
        if(true !== $result){
            // 验证失败 输出错误信息
            $this->error($result);exit;
        }

        $res = Db::name('pay_config')->insert($data);
        if($res){
          $this->success('添加成功',url('Aliyun/pay_config'));
        }
      }
      return $this->fetch('Aliyun/add_payconfig');
    }

    // 修改支付
    public function edit_payconfig(Request $request)
    {
      $this->view->engine->layout(false);
      $id = $this->request->param('id');
      $config = Db::name('pay_config')->where('id',$id)->field('id,pay_name,appid,appsecret,mchid,mchkey')->find();
      if($request->isPost()){
        $id = $this->request->post('id');
        $data = $this->request->post();
        $result = $this->validate($data,'Aliyun');
        if(true !== $result){
            // 验证失败 输出错误信息
            $this->error($result);exit;
        }

        $res = Db::name('pay_config')->where('id',$id)->update($data);
        if($res){
          $this->success('修改成功',url('Aliyun/pay_config'));
        }
      }
      return $this->fetch('Aliyun/edit_payconfig',['config'=>$config]);
    }

    // 启用支付
    public function pay_status()
    {
      $id = $this->request->post('id');
      # 需要优先关闭其他通道，再启动当前通道
      Db::name('pay_config')->where('status',1)->update(['status'=>0]);
      $res = Db::name('pay_config')->where('id',$id)->update(['status'=>1]);
      if($res){
        return json_encode(['status'=>1,'msg'=>'启用成功']);
      }else{
        return json_encode(['status'=>0,'msg'=>'启用失败']);
      }
    }

    // 删除支付
    public function del_pay()
    {
      $id = $this->request->post('id');
      # 判断是否有其他替换的支付
      $else_sms = Db::name('pay_config')->where('id','neq',$id)->field('id')->order('id desc')->find();
      if(empty($else_sms)) return json_encode(['status'=>0,'msg'=>'无替换配置，无法删除']);
      # 如果有替换的，优先开启替换的账号，然后删除
      Db::name('pay_config')->where('id',$else_sms['id'])->update(['status'=>1]);
      $res = Db::name('pay_config')->where('id',$id)->delete();
      if($res){
        return json_encode(['status'=>1,'msg'=>'删除成功']);
      }else{
        return json_encode(['status'=>0,'msg'=>'删除失败']);
      }
    }

}
