<?php
namespace app\admin\controller;
use app\common\controller\AdminBase;
use think\Request;
use think\Db;
/**
 * 微信控制器
 * @author yuqingyong
 */
class Weixin extends AdminBase
{
    // 微信配置页面
    public function wx_config()
    {
      // 配置列表
      $config_list = Db::name('weixin')->field('id,wx_name,appid,appsecret,mchid,mchkey,status')->select();

      $assign = [
        'config_list' => $config_list
      ];
      return $this->fetch('Weixin/wx_config',$assign);
    }

    // 添加公众号
    public function add_wx(Request $request)
    {
      $this->view->engine->layout(false);
      if($request->isPost()){
        $data = $this->request->post();

        $result = $this->validate($data,'Weixin');
        if(true !== $result){
            // 验证失败 输出错误信息
            $this->error($result);exit;
        }

        $res = Db::name('weixin')->insert($data);
        if($res){
          $this->success('添加成功',url('Weixin/wx_config'));
        }
      }
      return $this->fetch('Weixin/add_wx');
    }

    // 修改公众号
    public function edit_wx(Request $request)
    {
      $this->view->engine->layout(false);
      $id = $this->request->param('id');
      $weixin = Db::name('weixin')->where('id',$id)->field('id,appid,wx_name,appsecret,mchid,mchkey')->find();
      if($request->isPost()){
        $id = $this->request->post('id');
        $data = $this->request->post();
        $result = $this->validate($data,'Weixin');
        if(true !== $result){
            // 验证失败 输出错误信息
            $this->error($result);exit;
        }

        $res = Db::name('weixin')->where('id',$id)->update($data);
        if($res){
          $this->success('修改成功',url('Weixin/wx_config'));
        }
      }
      return $this->fetch('Weixin/edit_wx',['weixin'=>$weixin]);
    }

    // 启用公众号
    public function wx_status()
    {
      $id = $this->request->post('id');
      # 需要优先关闭其他通道，再启动当前通道
      Db::name('weixin')->where('status',1)->update(['status'=>0]);
      $res = Db::name('weixin')->where('id',$id)->update(['status'=>1]);
      if($res){
        return json_encode(['status'=>1,'msg'=>'启用成功']);
      }else{
        return json_encode(['status'=>0,'msg'=>'启用失败']);
      }
    }

    // 删除公众号
    public function del_wx()
    {
      $id = $this->request->post('id');
      # 判断是否有其他替换的公众号
      $else_wx = Db::name('weixin')->where('id','neq',$id)->field('id')->order('id desc')->find();
      if(empty($else_wx)) return json_encode(['status'=>0,'msg'=>'无替换公众号，无法删除']);
      # 如果有替换的，优先开启替换的账号，然后删除
      Db::name('weixin')->where('id',$else_wx['id'])->update(['status'=>1]);
      $res = Db::name('weixin')->where('id',$id)->delete();
      if($res){
        return json_encode(['status'=>1,'msg'=>'删除成功']);
      }else{
        return json_encode(['status'=>0,'msg'=>'删除失败']);
      }
    }
}
