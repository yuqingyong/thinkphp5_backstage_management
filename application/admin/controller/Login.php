<?php
namespace app\admin\controller;
use app\common\controller\AdminBase;
use app\admin\model\AdminUser;
use think\Request;
use think\Session;
use think\Cookie;
class Login extends AdminBase{
	
	public function _initialize(){}
	
	public function login(){	
	$this->view->engine->layout(false);	
		if(request()->ispost()){
			$data = $this->request->post();
			if(!captcha_check($data['code'])){
				$this->error('验证码错误');
			}else{
				// 实例化模型,检测用户账号密码
				$adminuser = new AdminUser();
				$result  = $adminuser->check_password($data['username'],$data['password']);
				if($result == false)
				{
					$this->error('登录失败,账号或者密码错误(或者被禁用了)');
				}else{
					Session::set('admin_user',$result);
					//更新登录时间和IP
					$gx['login_ip']    = get_real_ip();
					$gx['login_time'] = date('Y-m-d H:i:s',time());
					$adminuser->save([
						'login_time' => $gx['login_time'],
						'login_ip'   => $gx['login_ip'],
					],['id'=>$result['id']]);
					$this->redirect('Index/index');
				}
			}
			
		}
		return view('Login/login');
	}

	// 登出
	public function login_out()
	{
		Session::set('admin_user',null);
        $this->success('退出成功、前往登录页面', url('admin/Login/login'));
	}

	// 修改密码        md5(md5(input('post.password')).$salt)
	public function edit_pass(Request $request)
	{
		if($request->ispost()){
			if(md5(md5(input('post.mpass')).session('admin_user.salt')) == session('admin_user.password')) {
				$data['password'] = md5(md5(input('post.newpass')).input('post.salt'));
				$data['salt'] = $this->request->post('salt');
				$res = db('admin_user')->where('id',session('admin_user.id'))->field('password,salt')->update($data);
				if($res){$this->success('修改成功');exit;}
			}else{
				$this->error('原密码输入错误');exit;
			}
		}
		return view('Login/pass');
	}














}
