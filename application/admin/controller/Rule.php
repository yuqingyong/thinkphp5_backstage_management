<?php
namespace app\admin\controller;
use app\common\controller\AdminBase;
use app\admin\model\AdminRule;
use think\Db;
use think\request;
class Rule extends AdminBase{

	//管理员列表展示
	public function admin_user()
	{
		//读取管理员列表
		$admin = Db::table('yqy_admin_user')
		->alias('a')
		->join('yqy_auth_group w','a.group = w.id')
		->field('a.id,username,qq,login_time,login_ip,a.status,title')
		->select();

		$this->assign('data',$admin);
		return view('Rule/admin_user');
	}

	//添加管理员
	public function add_admin_user(Request $request)
	{
		$this->view->engine->layout(false);
		//添加管理员并返回自增ID
		if($request->ispost())
		{
			$group_id = input('post.group_id');
			$salt = input('post.salt');
			$data = [
				'username'=>input('post.username'),
				'password'=>md5(md5(input('post.password')).$salt),
				'qq'=>input('post.qq'),
				'email'=>input('post.email'),
				'salt'=>input('post.salt'),
				'status'=>input('post.status') ?: 1,
				'group'=>input('post.group_id'),
			];
			//返回刚添加的管理员ID
			$res = Db::name('admin_user')->insert($data);
			$userid = Db::name('admin_user')->getLastInsID();
			//根据管理员userid以及用户组group_id添加至用户组
			if($res == true){
				$date = [
					'uid'=>$userid,
					'group_id'=>$group_id
				];
				$resu = Db::name('auth_group_access')->insert($date);
				$this->success('添加成功');exit;
			}
			
		}
		$group = Db::table('yqy_auth_group')->select();
		$this->assign('group',$group);
		return view('Rule/add_admin');
	}

	//管理员更新
	public function edit_admin(Request $request)
	{
		$id = input('id');
		$admin_user = db('admin_user')->where('id',$id)->find();
		$this->assign('user_data',$admin_user);
		if($request->ispost())
		{
			$id = input('post.id');
			if(input('post.password') == ''){
				$group_id = input('post.group_id');
				$data = [
					'username'=>input('post.username'),
					'qq'=>input('post.qq'),
					'email'=>input('post.email'),
					'status'=>input('post.status'),
					'group'=>input('post.group_id'),
				];
				$res = db('admin_user')->where('id',$id)->update($data);
				//修改该用户所在组
				if($res > 0){
					$date = ['group_id'=>$group_id];
					$resu = Db::name('auth_group_access')->where('uid',$id)->update($date);
					$this->success('修改成功');exit;
				}
			}else{
				$group_id = input('post.group_id');
				$salt = input('post.salt');
				$data = [
					'username'=>input('post.username'),
					'password'=>md5(md5(input('post.password')).$salt),
					'qq'=>input('post.qq'),
					'email'=>input('post.email'),
					'salt'=>input('post.salt'),
					'status'=>input('post.status'),
					'group'=>input('post.group_id'),
				];
				$res = db('admin_user')->where('id',$id)->update($data);
				//修改该用户所在组
				if($res > 0){
					$date = ['group_id'=>$group_id];
					$resu = Db::name('auth_group_access')->where('uid',$id)->update($date);
					$this->success('修改成功');exit;
				}
			}
			
		}
		$group = Db::table('yqy_auth_group')->select();
		$this->assign('group',$group);
		return view('Rule/edit_admin');
	}



	//停用管理员
	public function admin_stop()
	{
		$id = input('id');
		$res= db('admin_user')->where('id',$id)->update(['status'=>0]);
		if($res>0){
			$arr = array('ok'=>'y');
			echo json_encode($arr);
		}
	}

	//启用管理员
	public function admin_start()
	{
		$id = input('id');
		$res= db('admin_user')->where('id',$id)->update(['status'=>1]);
		if($res>0){
			$arr = array('ok'=>'y');
			echo json_encode($arr);
		}
	}

	//删除管理员
	public function del_admin()
	{
		$id = input('id');
		$res= db('admin_user')->where('id',$id)->delete();
		if($res > 0){
			//如果删除了用户，则删除对应组关系
			db('auth_group_access')->where('uid',$id)->delete();
			return json_encode(['ok'=>'y']);
		}
	}

	//角色列表
	public function admin_group()
	{
		$group = Db::table('yqy_auth_group')->select();
		$this->assign('group',$group);
		return view('Rule/group');
	}
	
	//添加角色
	public function add_group(Request $request)
	{
		$this->view->engine->layout(false);
		if($request->ispost()){
			$data = ['title' => input('post.title'),'bak'=>input('post.bak')];
			$bool = DB::table('yqy_auth_group')->insert($data);
			if($bool == true){$this->success('添加成功');exit;}else{$this->error('添加失败');exit;}
		}
		
		return view('Rule/add_group');
	}
	
	// 删除角色
	public function del_group()
	{
		$group_id = input('post.id');
		# 查询组内是否存在用户
		$uid = Db::name('auth_group_access')->where('group_id',$group_id)->field('uid')->select();
		if($uid) return json_encode(['status'=>0,'msg'=>'组内还有用户，请先删除组内用户或者更改用户组']);
		# 否则直接删除
		$res = Db::name('auth_group')->where('id',$group_id)->delete();
		if($res){return json_encode(['status'=>1,'msg'=>'删除成功']);}else{json_encode(['status'=>0,'msg'=>'删除失败']);}
	}

	//分配权限
	public function rule_group(Request $request)
	{
		$this->view->engine->layout(false);
		$rule = new AdminRule();
		if($request->ispost())
		{
			//如果提交了数据则更新权限
			$id = input('post.id');
			$rule_ids = input('post.rule_ids/a');
			$data['rules']=implode(',', $rule_ids);
			$result = $rule->edit_rule($id,$data['rules']);
			if($result>0){$this->success('修改成功');exit;}else{$this->error('参数错误');exit;}
		}else{
			//查询分配者的信息
			$id = input('id');
			$group_data = DB::table('yqy_auth_group')->where('id',$id)->find();
			$group_data['rules']=explode(',', $group_data['rules']);
			//读取权限规则表
			$rule_data = $rule->rule_data();
            $assign=array(
                'group_data'=>$group_data,
                'rule_data'=>$rule_data
                );
            $this->assign($assign);
		}
		return view('Rule/rule_group');
	}



	public function auth()
	{
		//读取权限规则表
		$rule = new AdminRule();
		$rule_nav = $rule->rule_data();
		$this->assign('data',$rule_nav);
		return view('Rule/auth');
	}
	
	//修改权限
	public function edit_rule()
	{
		$this->view->engine->layout(false);
		//查询需要更改的信息
		$auth = AdminRule::where('id',input('id'))->find();
		if(request()->ispost())
		{
			$id    = input('post.id');
			$title = input('post.title');
			$name  = input('post.name');
			$res = AdminRule::where('id', $id)->update(['name' => $name,'title'=>$title]);
			if($res > 0)
			{
				$this->success('修改成功');
				exit;	
			}
		}
		
		return view('Rule/edit_auth',['auth'=>$auth]);
		
		
	}


	//添加权限规则
	public function add_rule()
	{
		$this->view->engine->layout(false);
		$pid = input('pid');
		if(request()->ispost())
		{
			if(empty($pid)){
				$pid   = 0;
			}else{
				$pid   = input('post.pid','0','int');
			}
			$title = input('post.title');
			$name  = input('post.name');

			// 使用model助手函数实例化AdminRule模型
			$user  = model('AdminRule');
			// 模型对象赋值
			$user->data([
			    'name'  =>  $name,
			    'title' =>  $title,
			    'pid'   => $pid
			]);
			$res = $user->save();
			if($res == true){$this->success('添加成功');exit;}
		}
		
		return view('Rule/add_auth',['pid'=>$pid]);
		
	}


	//删除权限规则
	public function delete()
	{
		$id = input('id');
		if(!empty($id)){
			$res= AdminRule::destroy($id);
			if($res > 0){return json_encode(['status'=>1,'msg'=>'删除成功']);}
		}else{
			return json_encode(['status'=>0,'msg'=>'参数错误']);
		}
		
	}




	
}
