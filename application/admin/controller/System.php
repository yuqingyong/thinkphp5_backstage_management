<?php
namespace app\admin\controller;
use app\common\controller\AdminBase;
use think\Request;
use think\Db;
/**
 * 系统设置控制器
 * @author yuqingyong
 */
class System extends AdminBase
{
    // 系统设置
    public function system_base(Request $request)
    {	
       # 获取系统基本设置
       $system_base = Db::name('system')->where('type',1)->field('type,config,id')->find();
       $system_base['config'] = json_decode($system_base['config'],true);
       # 修改设置
       if($request->isPost()){
          $data['title'] = $this->request->post('title','','htmlspecialchars');
          $data['keyword'] = $this->request->post('keyword','','htmlspecialchars');
          $data['describle'] = $this->request->post('describle','','htmlspecialchars');
          $data['copyright'] = $this->request->post('copyright','','htmlspecialchars');
          $data['beian'] = $this->request->post('beian','','htmlspecialchars');
          $id = $this->request->post('id');
          # 转化为json格式
          $config = json_encode($data);
          $res = Db::name('system')->where('id',$id)->update(['config'=>$config]);
          if($res){$this->success('修改成功',url('System/system_base'));exit;}else{$this->error('修改失败');}
       }

       return $this->fetch('System/system_base',['system'=>$system_base]);
    }

    // 系统日志
    public function system_log(Request $request)
    {   
       
        if($request->isPost()){
            # 检索
            $start_time = strtotime($this->request->post('start_time'));
            $end_time = strtotime($this->request->post('end_time'));
            # 查询
            $system_log = Db::name('system_log')
                    ->alias('a')
                    ->join('yqy_admin_user b','a.admin_id = b.id')
                    ->whereTime('op_time','between',[$start_time,$end_time])
                    ->field('op_type,op_time,op_bak,username,op_ip')->paginate(20);
        }else{
            $system_log = Db::name('system_log')
                    ->alias('a')
                    ->join('yqy_admin_user b','a.admin_id = b.id')
                    ->field('op_type,op_time,op_bak,username,op_ip')->paginate(20);
        }
        return $this->fetch('System/system_log',['system_log'=>$system_log]);
    }

    // 批量删除日志
    public function del_log()
    {
        $ids = json_decode($this->request->post('id'),true);
        foreach ($ids as $k => $v) {
            # 删除日志
            Db::name('system_log')->where('id',$v)->delete();
        }
        return json_encode(['status'=>1,'msg'=>'删除成功']);
    }


}
