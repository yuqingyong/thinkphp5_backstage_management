<?php
namespace app\admin\model;
use think\Db;
use think\Model;
/**	
 * 管理员模型
 */
class AdminUser extends Model
{
	//验证密码
	public function check_password($username = '' , $password = '')
	{
		$salt = DB::table('yqy_admin_user')->where('username',$username)->field('salt,password,group,status,username,id')->find();
		$user_pass = md5(md5($password).$salt['salt']);
		if($user_pass == $salt['password'] && $salt['status'] == 1){
			return $salt;
		}
		return false;
	}

	//查询管理员基本信息
	public function user($user_id,$field)
	{
		$data = DB::table('yqy_admin_user')->where('id',$user_id)->field($field)->find();
		return $data;
	}
	
}