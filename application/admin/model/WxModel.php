<?php
namespace app\admin\model;
use think\Db;
use think\Model;
/**	
 * 公众号模型
 */
class Wxmodel extends Model
{
	# 公众号appid
	static private  $appid;
	# 公众号appsecret
	static private  $appsecret;
	// 初始化
	protected function initialize()
	{
		# 获取配置
		$config = Db::name('weixin')->where('status',1)->order('id desc')->field('appid,appsecret,mchid,mchkey')->find();
		if($config){
			self::$appid = $config['appid'];
			self::$appsecret = $config['appsecret'];
		}else{
			return json(['status'=>0,'msg'=>'无任何启用的公众号配置']);
		}
	}
}