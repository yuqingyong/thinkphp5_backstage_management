<?php
namespace app\admin\model;
use think\Db;
use think\Model;
/**	
 * 权限规则模型
 */
class AdminRule extends Model{
	
	protected $table = 'yqy_auth_rule';
	
	//读取权限规则表
	public function rule_data()
	{
		$list= DB::table('yqy_auth_rule')->select();
		$rule = third_category($list,0);
		return $rule;
	}
	
	//编辑用户权限
	public function edit_rule($uid,$rules)
	{
		//更新权限数据
		$res = Db::table('yqy_auth_group')->update(['rules' => $rules,'id'=>$uid]);
		return $res;
	}
	
	/**
	 * 获取全部菜单
	 * @param  string $type tree获取树形结构 level获取层级结构
	 * @return array       	结构数据
	 */
	public function getTreeData($group){
		
		return $data;
	}
	
}