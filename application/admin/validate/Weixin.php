<?php 
namespace app\admin\validate;

use think\Validate;

class Weixin extends Validate
{
    protected $rule =   [
        'wx_name'  => 'require|max:25',
        'appid'   => 'require|max:18',
        'appsecret' => 'require|max:32',
    ];
    
    protected $message  =   [
        'name.require' => '名称必须',
        'name.max'     => '名称最多不能超过25个字符',
        'appid.require'   => 'appid必须',
        'appid.max'   => 'appid不能超过16位',
        'appsecret.require'   => 'appsecret必须',
        'appsecret.max'   => 'appsecret不能超过32位',
    ];
    
}

