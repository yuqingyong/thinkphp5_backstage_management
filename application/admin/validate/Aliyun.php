<?php 
namespace app\admin\validate;

use think\Validate;

class Aliyun extends Validate
{
    protected $rule =   [
        'appid'   => 'require|max:18',
        'appsecret' => 'require|max:32',
    ];
    
    protected $message  =   [
        'appid.require'   => 'appid必须',
        'appid.max'   => 'appid不能超过16位',
        'appsecret.require'   => 'appsecret必须',
        'appsecret.max'   => 'appsecret不能超过32位',
    ];
    
}

