星辰后台管理系统
===============
使用H-UI前端框架的后台管理系统，集合有微信公众号各个开发接口，包含auth权限管理，陆续会添加新的开发接口
该系统主要用于个人网站开发使用，因为之前发现当想要使用一个接口的时候需要到处去查询资料，很浪费时间，所
以我整合了一些接口的类文件，方便日后开发直接调用，加快开发速度。

### 项目中包含的一些composer文件可根据composer.json文件自行下载
	* 至于thinkphp的核心文件保持原样即可，因为文件过大，所以并未上传，也没有意义
	* 主要的代码文件都在application和public里面
	* 项目并非是完整的thinkphp5框架目录结构，请自行下载好thinkphp5的初始目录结构，再将本项目的相关文件移到
	  thinkphp5的项目中即可

### 文件目录
	|- application
		|- admin
			|- 包含控制器controller文件、view文件、model文件，主要是负责后台的编写和微信资料上传等接口
		|- home
			|- 网站的前台文件放置和编写，其中包含一个即时聊天功能的代码 [测试地址: http://www.yqyblog.cn/home/index/select_user]
		|- weixin
			|- 放置和微信公众号交互的控制器文件，微信开发接口文件
	|- vendor 
		|- overtrue 这是EasyWeChat的扩展文件，可自己使用composer下载，下方也说明了下载方式和使用方式
### EasyWecha3.X

### 一、概述和安装
	1.EasyWeChat是一个开源的微信非官方SDK
	2.环境需求
		- PHP >= 5.5.9(最好是php7)
		- PHP cURL 扩展
		- PHP OpenSSL 扩展
		- PHP fielinfo 拓展
	3.安装方式
		使用composer：
		$ composer require overtrue/wechat:~3.1 -vvv

#### 1.1 配置接口
	URL: http://weixin.yuqingyong.cn/weixin/Easychat/event
	Token：yuqingyong

	注意：由于使用的是thinkphp5的框架，所以在使用composer的时候，是不需要在入口文件添加
	include __DIR__ . '/vendor/autoload.php'; // 引入 composer 入口文件   这部操作是不需要的
	直接在控制器USE即可实例化类

	use EasyWeChat\Foundation\Application;
	***Composer自动加载***
	- 5.0版本支持Composer安装的类库的自动加载，你可以直接按照Composer依赖库中的命名空间直接调用。

### 二、网页聊天  swoole + websocket + redis
	1. 聊天文件放置于  application\home 文件夹下
	2. 服务端文件  server.php 在服务端使用php命令执行此文件，注意端口号，至于如何安装swoole可至swoole官网查看安装方式
		[swoole官网: https://wiki.swoole.com/wiki/page/6.html]
		执行php文件的命令：php server.php （cd到server.php文件目录下，然后直接执行即可，Ctrl+C可中断执行）
	3. 前端文件 在view目录的Index目录下，一个是 select_user.html文件，用于登录模拟用户，因为未使用mysql数据库，所以使
	   用的模拟的用户，后续可根据自己的需求编写真实用户
       另外一个文件是 web_socket.html文件，这里主要是聊天界面和CSS样式、JS代码的存放，为了方便查看，放到了一起，可分开
       存放。
    4. redis的使用方式
    	4.1：安装 
    		$ wget http://download.redis.io/releases/redis-2.8.17.tar.gz
			$ tar xzf redis-2.8.17.tar.gz
			$ cd redis-2.8.17
			$ make
		4.2：启动 (每个人使用的环境不一样，安装的位置也可能不一样，我使用的是lnmp环境，一般执行文件都存放于/usr/bin目录下)
			$ cd /usr/bin
			$ ./redis-cli 

		4.3：基本使用
			基本使用就不多说了，可查看redis的文档 [http://www.redis.net.cn/]
			项目重要用了redis的缓存，消息队列用于存储最多30条的消息记录（其实完全没必要的，只是为了练习redis的消息队列....消息记录完全可以使用js的本地缓存实现，或者有其他特殊需求可存储于数据库）
			
	5. 总结，这个网页聊天使用到的swoole的创建websocket服务器
	   WebSocket服务器是建立在Http服务器之上的长连接服务器，客户端首先会发送一个Http的请求与服务器进行握手。握手成功后会触发onOpen事件，表示连接已就绪，onOpen函数中可以得到$request对象，包含了Http握手的相关信息，如GET参数、Cookie、Http头信息等